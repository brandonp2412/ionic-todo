# Installing

To install the dependencies of this application, you need to run:

```sh
npm install
```

# Building

To build the project we must first package the JavaScript, and then head
over to [Android Studio](https://developer.android.com/studio/run) to run on an
emulator or physical device.

```sh
ionic build && ionic cap copy
```

We can open the project in Android Studio with:

```sh
ionic cap open android
```

Once inside android studio, run the application.

# References

- [Ionic](https://ionicframework.com/docs).
- [Android Studio](https://developer.android.com/docs).
