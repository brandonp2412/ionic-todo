import { IonCheckbox, IonItem, IonLabel, IonReorder } from "@ionic/react";
import React from "react";
import { Item } from "../data/item";

export const TodoItem: React.FC<{
  item: Item;
  onCheck: (item: Item) => void;
}> = ({ item, onCheck }) => {
  return (
    <IonItem>
      <IonLabel>{item.text}</IonLabel>
      <IonCheckbox
        slot="start"
        checked={false}
        onIonChange={() => onCheck(item)}
      ></IonCheckbox>
      <IonReorder slot="end" />
    </IonItem>
  );
};
