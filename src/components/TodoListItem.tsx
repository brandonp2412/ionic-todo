import { IonItem, IonLabel } from "@ionic/react";
import React from "react";
import Todo from "../data/todo";
import "./TodoListItem.css";

const TodoListItem: React.FC<{ todo: Todo }> = ({ todo }) => {
  return (
    <IonItem routerLink={`/todo/${todo.id}`} detail>
      <IonLabel className="ion-text-wrap">
        <h2>{todo.title}</h2>
      </IonLabel>
    </IonItem>
  );
};

export default TodoListItem;
