import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonList,
  IonPage,
  IonRefresher,
  IonRefresherContent,
  IonTitle,
  IonToolbar,
  useIonViewWillEnter,
} from "@ionic/react";
import { add, cloudDownload, cloudUpload } from "ionicons/icons";
import React, { useState } from "react";
import TodoListItem from "../components/TodoListItem";
import Todo from "../data/todo";
import { addTodo, getTodos, importTodos } from "../data/todo-service";
import "./Home.css";

const Home: React.FC = () => {
  const [todos, setTodos] = useState<Todo[]>([]);
  const [text, setText] = useState<string>();

  useIonViewWillEnter(async () => {
    const newTodos = await getTodos();
    setTodos([...newTodos]);
  });

  const handleTextKey = async (key: string) => {
    if (key !== "Enter" || !text) return;
    await addTodo(text);
    setText("");
    const newTodos = await getTodos();
    setTodos([...newTodos]);
  };

  const refresh = (e: CustomEvent) => {
    setTimeout(() => {
      e.detail.complete();
    }, 3000);
  };

  const handleDownload = () => {
    const data = todos
      .map((todo) => {
        let text = todo.title + ":\n";
        text += todo.items?.map((item) => `- ${item.text}`).join("\n");
        return text;
      })
      .join("\n\n");
    const file = new Blob([data], { type: "text/plain" });
    const a = document.createElement("a"),
      url = URL.createObjectURL(file);
    a.href = url;
    a.download = "todos.txt";
    document.body.appendChild(a);
    a.click();
    setTimeout(function () {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 0);
  };

  const handleUpload = () => {
    const fileInput = document.getElementById("file-input");
    fileInput?.click();
  };

  const handleFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e.target.files || e.target.files.length !== 1) return;
    await importTodos(e.target.files[0]);
    setTodos(await getTodos());
  };

  return (
    <IonPage id="home-page">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Todos</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={handleUpload}>
              <input
                type="file"
                hidden
                id="file-input"
                onChange={handleFile}
              ></input>
              <IonIcon icon={cloudUpload}></IonIcon>
            </IonButton>
            <IonButton onClick={handleDownload}>
              <IonIcon icon={cloudDownload}></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonRefresher slot="fixed" onIonRefresh={refresh}>
          <IonRefresherContent></IonRefresherContent>
        </IonRefresher>

        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Todo</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonList>
          {todos.map((todo) => (
            <TodoListItem key={todo.id} todo={todo} />
          ))}
          <IonItem>
            <IonIcon
              slot="start"
              icon={add}
              onClick={() => handleTextKey("Enter")}
            />
            <IonInput
              value={text}
              onIonChange={(e) => setText(e.detail.value!)}
              onKeyUp={(e) => handleTextKey(e.key)}
              autocapitalize="on"
            ></IonInput>
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Home;
