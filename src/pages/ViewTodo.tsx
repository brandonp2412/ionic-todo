import { ItemReorderEventDetail } from "@ionic/core";
import {
  IonAlert,
  IonBackButton,
  IonButtons,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonPage,
  IonReorderGroup,
  IonToast,
  IonToolbar,
  useIonViewWillEnter,
} from "@ionic/react";
import { History } from "history";
import { add, trash } from "ionicons/icons";
import React, { useState } from "react";
import { RouteComponentProps } from "react-router";
import { TodoItem } from "../components/TodoItem";
import { Item } from "../data/item";
import Todo from "../data/todo";
import { addItem, getTodo, removeTodo, updateTodo } from "../data/todo-service";

interface ViewTodoProps extends RouteComponentProps<{ id: string }> {
  history: History;
}

const ViewTodo: React.FC<ViewTodoProps> = ({ match, history }) => {
  const [todo, setTodo] = useState<Todo>();
  const [oldTodo, setOldTodo] = useState<Todo>();
  const [checkedText, setCheckedText] = useState<string>();
  const [text, setText] = useState<string>();
  const [showRemoved, setShowRemoved] = useState<boolean>(false);
  const [showConfirm, setShowConfirm] = useState<boolean>(false);

  useIonViewWillEnter(async () => {
    const newTodo = await getTodo(match.params.id);
    setTodo({ ...newTodo } as Todo);
  });

  const remove = async (item: Item) => {
    const todoCopy = { ...todo } as Todo;
    todoCopy.items = todo?.items?.filter((i) => i.id !== item.id);
    await updateTodo(todoCopy);
    setOldTodo(todo);
    setCheckedText(item.text);
    const newTodo = await getTodo(match.params.id);
    setTodo({ ...newTodo } as Todo);
    setShowRemoved(true);
  };

  const handleTextKey = async (key: string) => {
    if (key !== "Enter") return;
    await addItem(match.params.id, text!);
    const newTodo = await getTodo(match.params.id);
    setTodo(newTodo);
    setText("");
  };

  const handleRemove = async () => {
    await removeTodo(match.params.id);
    history.goBack();
  };

  const reorder = async (event: CustomEvent<ItemReorderEventDetail>) => {
    const items = [...todo?.items!];
    const from = items[event.detail.from];
    items[event.detail.from] = items[event.detail.to];
    items[event.detail.to] = from;
    const newTodo = { ...todo, items } as Todo;
    await updateTodo(newTodo);
    event.detail.complete();
  };

  return (
    <IonPage id="view-todo-page">
      <IonHeader translucent>
        <IonToolbar>
          <IonButtons>
            <IonBackButton
              text={todo?.title}
              defaultHref="/home"
            ></IonBackButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonReorderGroup disabled={false} onIonItemReorder={reorder}>
          {todo?.items?.map((item: Item, index: number) => (
            <TodoItem item={item} key={index} onCheck={remove} />
          ))}
          <IonItem>
            <IonIcon
              slot="start"
              icon={add}
              onClick={() => handleTextKey("Enter")}
            />
            <IonInput
              value={text}
              onIonChange={(e) => setText(e.detail.value!)}
              onKeyUp={(e) => handleTextKey(e.key)}
              autocapitalize="on"
            ></IonInput>
          </IonItem>
        </IonReorderGroup>

        <IonAlert
          isOpen={showConfirm}
          onDidDismiss={() => setShowConfirm(false)}
          header="Delete todo"
          message={`Are you sure you want to delete ${todo?.title}?`}
          buttons={[
            {
              text: "Cancel",
              role: "cancel",
            },
            {
              text: "Okay",
              handler: handleRemove,
            },
          ]}
        />

        <IonToast
          isOpen={showRemoved}
          onDidDismiss={() => setShowRemoved(false)}
          message={`Ticked ${checkedText}`}
          buttons={[
            {
              side: "end",
              icon: "undo",
              text: "Undo",
              role: "cancel",
              handler: async () => {
                await updateTodo(oldTodo!);
                setOldTodo(undefined);
                const newTodo = await getTodo(match.params.id);
                setTodo(newTodo);
              },
            },
          ]}
          duration={3000}
        />

        <IonFab vertical="bottom" horizontal="end" slot="fixed">
          <IonFabButton color="danger" onClick={() => setShowConfirm(true)}>
            <IonIcon icon={trash} />
          </IonFabButton>
        </IonFab>
      </IonContent>
    </IonPage>
  );
};

export default ViewTodo;
