import { Storage } from "@capacitor/core";
import { v4 as uuidv4 } from "uuid";
import { readText } from "../pages/file-service";
import Todo from "./todo";

export const getTodos = async (): Promise<Todo[]> => {
  const response = await Storage.get({ key: "todos" });
  if (response.value !== null) return JSON.parse(response.value);
  await setTodos([]);
  return [];
};

const setTodos = (todos: Todo[]) =>
  Storage.set({ key: "todos", value: JSON.stringify(todos) });

export const getTodo = async (id: string) => {
  const todos = await getTodos();
  return todos.find((m) => m.id === id);
};

export const addTodo = async (title: string): Promise<void> => {
  const todos = await getTodos();
  todos.push({
    title,
    id: uuidv4(),
    items: [],
  });
  return setTodos(todos);
};

export const importTodos = async (file: File) => {
  const text = await readText(file);
  const rawTodos = text.split("\n\n").map((todo) => todo.split("\n"));
  const newTodos: Todo[] = rawTodos.map((todo) => ({
    id: uuidv4(),
    title: todo[0].replace(/:$/, ""),
    items: todo
      .slice(1)
      .map((item) => item.replace(/^- /, ""))
      .map((item) => ({ id: uuidv4(), text: item })),
  }));
  const todos = await getTodos();
  todos.push(...newTodos);
  return setTodos(todos);
};

export const updateTodo = async (todo: Todo) => {
  let todos = await getTodos();
  todos = todos.map((t) => {
    if (t.id !== todo.id) return t;
    return todo;
  });
  return setTodos(todos);
};

export const addItem = async (id: string, text: string): Promise<void> => {
  let todos = await getTodos();
  const todo = todos.find((t) => t.id === id);
  todo!.items?.push({
    id: uuidv4(),
    text,
  });
  todos = todos.map((t) => {
    if (t.id !== id) return t;
    return todo;
  }) as Todo[];
  return setTodos(todos);
};

export const removeTodo = async (id: string): Promise<void> => {
  let todos = await getTodos();
  todos = todos.filter((todo) => todo.id !== id);
  return setTodos(todos);
};
