import { Item } from "./item";

export default interface Todo {
  id: string;
  title: string;
  items?: Item[];
}
